﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TALProject.Models;
using TALProject.Services;

namespace UnitTest
{
    [TestClass]
    public class FootballTeamServiceTest
    {
        private IComparerService comparerService;
        private IEnumerable<ITeamResult> teams;
        public FootballTeamServiceTest()
        {
            comparerService=new FootballTeamService();
            //prepare data for testing
            ITeamResult team1 = new FootballTeam();
            team1.Name = "Team_01";
            team1.AValue = 50;
            team1.FValue = 30;

            ITeamResult team2 = new FootballTeam();
            team2.Name = "Team_02";
            team2.AValue = 51;
            team2.FValue = 32;

            teams = new List<ITeamResult>(2) {team1, team2};
        }

        [TestMethod]
        public void Test_CompareScore()
        {
            var result = comparerService.CompareScore(teams);
            Assert.AreEqual(51,result.AValue);
            Assert.AreEqual(32, result.FValue);
        }
    }
}
