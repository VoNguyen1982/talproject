﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TALProject.Enums;
using TALProject.Helpers;

namespace UnitTest
{
    [TestClass]
    public class HelperTest
    {
        public HelperTest()
        {
        }

        [TestMethod]
        public void Test_CSV()
        {
            var result = FileHelper.GetExtention("football.csv");
            Assert.AreEqual(FileType.CSV,result);
        }

        [TestMethod]
        public void Test_DAT()
        {
            var result = FileHelper.GetExtention("football.dat");
            Assert.AreEqual(FileType.DAT, result);
        }

        [TestMethod]
        public void Test_RemoveDoubleSpaces()
        {
            var result =
                MappingHelper.RemoveDoubleSpaces("    1.   ManUTD        2    3     4     5    6    7       7     ");
            Assert.AreEqual("1. ManUTD 2 3 4 5 6 7 7",result);
        }

        [TestMethod]
        public void Test_RemoveRecordNo()
        {
            var result =
                MappingHelper.RemoveRecordNo("1. ManUTD 2 3 4 5 6 7 7");
            Assert.AreEqual("ManUTD 2 3 4 5 6 7 7", result);
        }

        [TestMethod]
        public void Test_Mapping()
        {
            var result =
                MappingHelper.MappingToObject("ManUTD 1 2 3 4 5 6 7 8 9"," ");
            Assert.AreEqual("ManUTD", result.Name);
            Assert.AreEqual(5, result.FValue);
            Assert.AreEqual(7, result.AValue);
        }

    }
}
