﻿using System.Collections.Generic;
using TALProject.Enums;
using TALProject.Helpers;
using TALProject.Models;
using TALProject.Services;

namespace TALProject.Processes
{
    public class FileProcess 
    {
        FactoryService factoryService = null;
        DataService service = null;

        public FileProcess()
        {
            factoryService = new FactoryService();
        }

        public IEnumerable<ITeamResult> ReadFile(string fileName)
        {
            FileHelper.CheckExist(fileName);
            FileType fileType = FileHelper.GetExtention(fileName);
            service = factoryService.GetService(fileType.ToString());
            service.FileName = fileName;
            service.Read();
            return service.Results;
        }

        
    }
}
