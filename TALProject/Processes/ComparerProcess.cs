﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TALProject.Models;
using TALProject.Services;

namespace TALProject.Processes
{
     
    public class ComparerProcess
    {
        private IComparerService comparerService;

        public ComparerProcess(IComparerService comparerService)
        {
            this.comparerService = comparerService;
        }

        public ITeamResult Compare(IEnumerable<ITeamResult> teams)
        {
            return comparerService.CompareScore(teams);
        }
    }
}
