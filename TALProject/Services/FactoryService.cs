﻿using System;
using System.Collections.Generic;
using System.Reflection;


namespace TALProject.Services
{
    //selecting a service based on the file extension

    public abstract class SimpleService<TEnity>
    {
        public abstract TEnity GetService(string type);
    }

    public class FactoryService : SimpleService<DataService>
    {
        Dictionary<string, DataService> services = new Dictionary<string, DataService>();

        public FactoryService()
        {
           Assembly assembly = Assembly.GetExecutingAssembly();
           Type[] types=   assembly.GetTypes();
           foreach(Type type in types)
           {
               if (type.IsSubclassOf(typeof(DataService)) && (!type.IsAbstract))
               {
                   DataService service = Activator.CreateInstance(type) as DataService;
                   services.Add(service.ToString(), service);
               }
           }
        }
        public override DataService GetService(string type)
        {
            return services[type];
        }
    }

    
}
