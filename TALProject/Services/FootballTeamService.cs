﻿using System;
using System.Collections.Generic;
using TALProject.Models;


namespace TALProject.Services
{
    public interface IComparerService
    {
        ITeamResult CompareScore(IEnumerable<ITeamResult> teams);
    }

    //compare A and F scores of teams

    public class FootballTeamService : IComparerService
    {
        public ITeamResult CompareScore(IEnumerable<ITeamResult> teams)
        {
            ITeamResult result = new FootballTeam();
            int score = int.MaxValue;
            foreach (var team in teams)
            {
                if (team.ErrorMessages == null && Math.Abs(team.FValue - team.AValue) < score)
                {
                    result = team;
                    score = Math.Abs(team.FValue - team.AValue);
                }
            }
            return result;
        }
    }
}
