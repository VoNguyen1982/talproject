﻿using TALProject.Enums;

namespace TALProject.Services
{
    // read the csv file

   public  class CSVDataService:DataService 
    {
       public CSVDataService(string FileName):base(FileName)
       {
       }
       public CSVDataService()
           : base()
       {
       }
        public override void Read()
        {
            Divider = ",";
            base.Read();
        }
        public override string ToString()
        {
            return FileType.CSV.ToString();
        }
    }
}
