﻿using TALProject.Enums;

namespace TALProject.Services
{
    /// <summary>
    /// Read the dat file
    /// </summary>
    public class DATDataService : DataService
    {

        public DATDataService(string FileName):base(FileName)
        {
          
        }
        public DATDataService()
           : base()
        {
        }
        public override void Read()
        {

            Divider = " ";
            base.Read();
        }
       
        public override string ToString()
        {
            return FileType.DAT.ToString();
        }
    }
}
