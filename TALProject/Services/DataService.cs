﻿using System.Collections.Generic;
using System.IO;
using TALProject.Helpers;
using TALProject.Models;

namespace TALProject.Services
{
   public abstract class DataService
    {
       public DataService()
       {
           Results = new List<ITeamResult>();
       }

       public DataService(string FileName)
           : this()
       {
           this.FileName = FileName;
       }
       public virtual string Divider { get; set; }
      
       public virtual void Read()
       {
           using (FileStream stream = new FileStream(FileName, FileMode.OpenOrCreate, FileAccess.Read))
           {
               using (StreamReader reader = new StreamReader(stream))
               {
                   string line = reader.ReadLine(); // ignore the header

                   while ((line = reader.ReadLine()) != null)
                   {
                       line = MappingHelper.RemoveRecordNo(line);
                       line = MappingHelper.RemoveDoubleSpaces(line);
                       ITeamResult emp = MappingHelper.MappingToObject(line, Divider);
                       TeamHelper.Validate(emp);
                       Results.Add(emp);
                   }
              }
           }
       }
      public string FileName { get; set; }

      public  List<ITeamResult> Results { get; set; }
    }
}
