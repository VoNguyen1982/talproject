﻿using System;
using System.IO;
using TALProject.Enums;

namespace TALProject.Helpers
{
    public class FileHelper 
    {
        public static void CheckExist(string fileName)
        {
            if (!File.Exists(fileName))
                throw new Exception("The file does not exist.");
        }

        public static FileType GetExtention(string FileName)
        {
            string strFileType = Path.GetExtension(FileName).ToLower();
            switch (strFileType)
            {
                case ".csv":
                    return FileType.CSV;
                default:
                    return FileType.DAT;
            }
        }
    }
}
