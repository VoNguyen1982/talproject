﻿using System;
using System.Collections.Generic;
using TALProject.Models;

namespace TALProject.Helpers
{
    public class MappingHelper
    {
        public static ITeamResult MappingToObject(string line, string divider)
        {
            try
            {
                string[] valueStrings = line.Split(divider.ToCharArray());
                ITeamResult footballTeam = new FootballTeam()
                {
                    Name = valueStrings[0],
                    FValue =
                        Convert.ToInt32((string.IsNullOrEmpty(valueStrings[5])
                            ? "0"
                            : valueStrings[5])),
                    AValue =
                        Convert.ToInt32((string.IsNullOrEmpty(valueStrings[7])
                            ? "0"
                            : valueStrings[7])),
                };
                return footballTeam;
            }
            catch (Exception ex)
            {
                return new FootballTeam() {ErrorMessages = new List<string>(1) {ex.Message}};
            }
        }

        public static string RemoveDoubleSpaces(string line)
        {
            while (line.IndexOf("  ") > -1)
            {
                line = line.Replace("  ", " ").Trim();
            }
            return line;
        }

        public static string RemoveRecordNo(string line)
        {
            try
            {
                line = line.Trim();
                line = line.Substring(line.IndexOf(" ")).Trim();
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
            return line;
        }
    }
}
