﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TALProject.Models;


namespace TALProject.Helpers
{
    internal class TeamHelper
    {
        public static void Validate(ITeamResult team)
        {
            var results = new List<ValidationResult>();
            List<string> errors = new List<string>();
            ValidationContext context = new ValidationContext(team);

            if (!Validator.TryValidateObject(team, context, results))
            {
                foreach (var validationResult in results)
                {
                    errors.Add(validationResult.ErrorMessage);
                }
                team.ErrorMessages = errors;
            }
        }
    }
}
