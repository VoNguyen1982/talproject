﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TALProject.Models
{
    public class FootballTeam : ITeamResult
    {
        [Required(ErrorMessage = "The Name is empty")]
        public string Name
        {
            get;
            set;

        }

        [Required(ErrorMessage = "The F column value is empty or invalid")]
        public int FValue
        {
            get;

            set;

        }

        [Required(ErrorMessage = "The A column value is empty or invalid")]
        public int AValue
        {
            get;

            set;

        }

        public List<string> ErrorMessages
        {
            get;

            set;
        }
    }
}
