﻿using System.Collections.Generic;

namespace TALProject.Models
{
    public interface ITeam
    {
        string Name { get; set; }
        int FValue { get; set; }
        int AValue { get; set; }
    }

    public interface ITeamResult : ITeam
    {
        List<string> ErrorMessages { get; set; }
    }
    
}
