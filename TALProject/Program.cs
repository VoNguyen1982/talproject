﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TALProject.Helpers;
using TALProject.Processes;
using TALProject.Services;

namespace TALProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //read file and mapping to model
            FileProcess process = new FileProcess();
            var dataset = process.ReadFile(ConfigurationManager.AppSettings["FileName"]);
            
            //process the comparation
            ComparerProcess cProcess = new ComparerProcess(new FootballTeamService());
            var result = cProcess.Compare(dataset);

            
            if (result.ErrorMessages == null)
            {
                Console.WriteLine("{0} {1} {2}", result.Name, result.FValue, result.AValue);
            }
            else
            {
                Console.WriteLine(result.ErrorMessages[0]);
            }

            Console.ReadLine();
        }
    }
}
