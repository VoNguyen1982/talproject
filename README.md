Dear Sir/Madam,

In order to make the project working, the correct path needs to be configured in app.config.

The program supports both csv and dat file extension.
 
Basically, the code was separated into small units as Services, Processes and Models and Helpers etc. The SOLID design pattern has also been applied.

There are two classes (CSVDataService and DATDataService) that are diverted from an abstract DataServices class. The main purposes of these classes are reading and mapping raw data (csv and dat files) to FootballTeam objects.

If there is a new file extension then a new diverted one will need to be implemented as a child class of DataService without modifying existing classes.

FactoryService class is a diverted class from an abstract SimpleService class which helps to select CSVDataService or DATDataService based on file extensions.

FileProcess\Readfile() method processes data by using above services/helpers and returns a set of FootballTeam objects. After that, the ComparerProcess\Compare() method will execute FootballTeamService\CompareScore() to find the final result.

I made some assumptions about data file structure

-	There is always at least 1 space in between Record No and Team Name
-	The order of columns is unchanged
-	No space or divider character in team name
-	Only processing data of columns : Team name, F and A columns.

For DAT file processing, all extra white spaces in the DAT file had been removed by helpers and there is only 1 space between 2 columns. After that, the data will be divided by blank space. 

In order to test, some test cases had been implemented. The most important method is DataService\Read() but all helpers used in this method have been covered by unit test.

On the other hands, try-catch had been used to catch exceptions. In my current job, I created an ILog interface with different implementations allowing to log error messages to different log service as Splunk and Windows Event.


I am looking forward to seeing the feedback.

Thanks
Vo Nguyen
